const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.version();

/**
 * App
 */
mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .extract();

/**
 * Display
 */
mix.js('resources/js/display.js', 'public/js')
    .sass('resources/sass/display.scss', 'public/css')
    .extract();

mix.copyDirectory('node_modules/@fortawesome/fontawesome-free/webfonts', 'public/font/fontawesome');
