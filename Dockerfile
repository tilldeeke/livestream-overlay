FROM alpine:3.13
LABEL maintainer="Till Deeke <kontakt@tilldeeke.de>" \
      description="A complete container for the app"

# Adapted from https://github.com/TrafeX/docker-php-nginx/blob/master/Dockerfile and https://github.com/jabardigitalservice/docker-phpfpm-nginx/blob/master/7.4/Dockerfile

# Install packages
RUN apk add --no-cache \
    php7 php7-fpm php7-session php7-phar php7-curl php7-dom \
    php7-bcmath php7-ctype php7-json php7-mbstring php7-openssl php7-pdo php7-pdo_sqlite php7-xml \
    php7-fileinfo php7-tokenizer \
    nginx supervisor curl nodejs npm

# Remove default nginx config
RUN rm /etc/nginx/conf.d/default.conf

# Configure nginx
COPY docker/nginx/nginx.conf /etc/nginx/nginx.conf
RUN chmod 777 /etc/nginx/nginx.conf

# Configure PHP-FPM
COPY docker/php-fpm/fpm-pool.conf /etc/php7/php-fpm.d/www.conf
RUN chmod 777 /etc/php7/php-fpm.d/www.conf
COPY docker/php-fpm/php.ini /etc/php7/conf.d/custom.ini
RUN chmod 777 /etc/php7/conf.d/custom.ini

# Configure supervisord
COPY docker/supervisord/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
RUN chmod 777 /etc/supervisor/conf.d/supervisord.conf

# Setup document root
RUN mkdir -p /var/www/html

RUN mkdir -p /.npm

# Make sure files/folders needed by the processes are accessable when they run under the nobody user
RUN chown -R nobody.nobody /var/www/html && \
  chown -R nobody.nobody /run && \
  chown -R nobody.nobody /var/lib/nginx && \
  chown -R nobody.nobody /var/log/nginx && \
  chown -R nobody.nobody /.npm

# Configure startup
COPY docker/startup.sh /startup.sh
RUN chmod +x /startup.sh && \
    chown -R nobody.nobody /startup.sh

# Switch to use a non-root user from here on
USER nobody

# Add application
WORKDIR /var/www/html/
COPY --chown=nobody . /var/www/html/

# Install composer dependencies
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
RUN composer install --optimize-autoloader --no-dev

# Generate assets
RUN npm install
RUN npm run production

# Expose the port nginx is reachable on
EXPOSE 8080 6001

# Let supervisord start nginx & php-fpm
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]

# Configure a healthcheck to validate that everything is up&running
HEALTHCHECK --timeout=10s CMD curl --silent --fail http://127.0.0.1:8080/fpm-ping
