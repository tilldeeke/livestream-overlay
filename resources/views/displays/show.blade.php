@extends('layouts.display')

@section('content')
    <iframe src="{{ route('countdowns.display') }}" frameborder="0" style="position:absolute;top:0;left:0;width:60%;height:60%" scrolling="no"></iframe>
    @foreach($display->tallyLights as $light)
        <iframe src="{{ route('tally-lights.show', $light) }}" frameborder="0" style="position:absolute;top:0;right:0;width:40%;height:60%" scrolling="no"></iframe>
    @endforeach
    <iframe src="{{ route('scene-status.display') }}" frameborder="0" style="border-top: 10px solid black;position:absolute;bottom:0;right:0;width:100%;height:40%" scrolling="no"></iframe>
@endsection
