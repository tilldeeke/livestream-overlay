@extends('layouts.app')

@section('content')
    <x-page-header title="Bildschirme" />

    <fieldset>
        <legend>Neues Display</legend>
        <form action="{{ route('displays.store') }}" method="POST">
            @csrf
            <table>
                <tr>
                    <td><label for="name">Name:</label></td>
                    <td>
                        <input type="text" name="name" id="name" value="{{ old('name') }}">
                        @error('name')
                        <p>{{ $message }}</p>
                        @enderror
                    </td>
                </tr>
                <tr>
                    <td><label for="tally_lights">Tally Lights:</label></td>
                    <td>
                        <select name="tally_lights[]" id="tally_lights" multiple>
                        @foreach($tallyLights as $light)
                            <option value="{{ $light->id }}">{{ $light->name }}</option>
                        @endforeach
                        </select>
                        @error('tally_lights')
                        <p>{{ $message }}</p>
                        @enderror
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><button type="submit">Save</button></td>
                </tr>
            </table>
        </form>
    </fieldset>
    <table border="1">
        <thead>
        <tr>
            <th>Displays</th>
            <th>Tally Lights</th>
        </tr>
        </thead>
        <tbody>
            @foreach($displays as $display)
                <tr>
                    <td><a href="{{ route('displays.show', $display) }}">{{ $display->name }}</a></td>
                    <td>{{ $display->tallyLights->pluck('name')->implode(', ') }}</td>
                    <td>
                        <form action="{{ route('displays.destroy', $display) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit">X</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <!--
    <div id="trash" style="width:200px;height:200px;background:red">
        Trash
    </div>
    <div class="newWidget grid-stack-item" style="width:200px" data-id="COUNT">
        <div class="grid-stack-item-content">
            COUNTDOWN
        </div>
    </div>
    <div style="width: 800px">
        <h1>Serialization demo</h1>
        <a onClick="saveGrid()" class="btn btn-primary" href="#">Save</a>
        <a onClick="loadGrid()" class="btn btn-primary" href="#">Load</a>
        <a onClick="clearGrid()" class="btn btn-primary" href="#">Clear</a>
        <br/><br/>
        <div class="grid-stack"></div>
        <hr/>
        <textarea id="saved-data" cols="100" rows="20" readonly="readonly"></textarea>
    </div>
    -->
@endsection

@push('styles')
    <style>
        .grid-stack {
            background: lightgoldenrodyellow;
        }

        .grid-stack-item-content {
            color: #2c3e50;
            text-align: center;
            background-color: #18bc9c;
        }
    </style>
@endpush

@push('scripts')
<!--
    <script type="text/javascript">
        // TODO: switch jquery-ui out
        $('.newWidget').draggable({
            revert: 'invalid',
            scroll: false,
            appendTo: 'body',
            helper: 'clone'
        });

        var grid = window.GridStack.init({

            resizable: {
                handles: 'n, ne, e, se, s, sw, w, nw'
            },
            acceptWidgets: '.newWidget',
            removable: '#trash',
            removeTimeout: 10,
            column: 12,
            maxRow: 12,
        });

        grid.on('added', function(e, items) {log('added ', items)});
        grid.on('removed', function(e, items) {log('removed ', items)});
        grid.on('change', function(e, items) {log('change ', items)});
        function log(type, items) {
            var str = '';
            items.forEach(function(item) { str += ' (x,y)=' + item.x + ',' + item.y; });
            console.log(type + items.length + ' items.' + str );
        }

        var serializedData = [
            {x: 0, y: 0, width: 2, height: 2, data: {id: '12'}},
            {x: 3, y: 1, width: 1, height: 2, data: {id: '34'}},
            {x: 4, y: 1, width: 1, height: 1, data: {id: '56'}},
            {x: 2, y: 3, width: 3, height: 1, data: {id: '78'}},
            {x: 1, y: 3, width: 1, height: 1, data: {id: '90'}}
        ];

        loadGrid = function() {
            grid.removeAll();
            var items = window.GridStack.Utils.sort(serializedData);
            console.log(items);
            grid.batchUpdate();
            items.forEach(function (node) {
                grid.addWidget('<div data-id="' + node.data.id + '"><div class="grid-stack-item-content">' + node.data.id + '</div></div>', node);
            });
            grid.commit();
        };

        saveGrid = function() {
            serializedData = [];
            grid.engine.nodes.forEach(function(node) {
                console.log(node.el);
                serializedData.push({
                    x: node.x,
                    y: node.y,
                    width: node.width,
                    height: node.height,
                    data: {
                        id: node.el.getAttribute('data-id')
                    }
                });
            });
            document.querySelector('#saved-data').value = JSON.stringify(serializedData, null, '  ');
        };

        clearGrid = function() {
            grid.removeAll();
        }

        loadGrid();
    </script>
    -->
@endpush
