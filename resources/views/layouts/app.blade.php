<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CSD Livestream Overlay</title>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    @livewireStyles
    @stack('styles')
</head>
<body>
    <main class="container-lg mt-5">
        <div class="row">
            <aside class="col-lg-2">
                <ul class="nav flex-column nav-pills mb-5">
                    <li class="nav-item">
                        <a class="nav-link {{ Request::routeIs('overview.*')?'active':'' }}" href="{{ route('overview.index') }}">Übersicht</a>
                    </li>
                </ul>
                <p class="h6 text-uppercase text-black-50">Stream Widgets</p>
                <ul class="nav flex-column nav-pills mb-5">
                    <li class="nav-item">
                        <a class="nav-link {{ Request::routeIs('lower-thirds.*')?'active':'' }}" href="{{ route('lower-thirds.index') }}">Bauchbinden</a>
                    </li>
                </ul>
                <p class="h6 text-uppercase text-black-50">Control Deck</p>
                <ul class="nav flex-column nav-pills mb-5">
                    <li class="nav-item">
                        <a class="nav-link {{ Request::routeIs('countdowns.*')?'active':'' }}" href="{{ route('countdowns.index') }}">Countdown</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ Request::routeIs('tally-lights.*')?'active':'' }}" href="{{ route('tally-lights.index') }}">Tally Lights</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ Request::routeIs('scenes.*')?'active':'' }}" href="{{ route('scenes.index') }}">Scenes</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ Request::routeIs('displays.*')?'active':'' }}" href="{{ route('displays.index') }}">Displays</a>
                    </li>
                </ul>
                <p class="h6 text-uppercase text-black-50">Einstellungen</p>
                <ul class="nav flex-column nav-pills mb-5">
                    <li class="nav-item">
                        <a class="nav-link {{ Request::routeIs('settings.general*')?'active':'' }}" href="{{ route('settings.general') }}">Allgemein</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ Request::routeIs('settings.services*')?'active':'' }}" href="{{ route('settings.services') }}">Dienste</a>
                    </li>
                </ul>
            </aside>
            <div class="col-lg-9 offset-lg-1">

                @if(session()->has('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>Erfolg!</strong> {{ session()->get('success') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                @yield('content')
            </div>
        </div>
    </main>

    <!-- Scripts: App -->
    <script src="{{ mix('js/manifest.js') }}"></script>
    <script src="{{ mix('js/vendor.js') }}"></script>
    <script src="{{ mix('js/app.js') }}"></script>

    <!-- Scripts: Stack -->
    @stack('scripts')

    <!-- Scripts: Livewire -->
    @livewireScripts
</body>
</html>
