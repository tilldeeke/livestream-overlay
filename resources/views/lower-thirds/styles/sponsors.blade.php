<section class="sponsors">
    <div class="sponsors_logos">
        <div style="margin-right: 24px">
            <img src="/glide/lhh.jpg?h=100" alt="">
        </div>
        <div>
            <img src="/glide/qnn_kampagne.png?h=100" alt="">
            <div style="font-size: 12px">gefördert aus Mitteln des Nieders. Ministeriums für Soziales, Gesundheit und Gleichstellung</div>
        </div>
    </div>
    <div class="sponsors_title">Gefördert durch</div>
</section>
