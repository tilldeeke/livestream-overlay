<section class="lower-third_two-people">
    <div class="person-1">
        @if($lowerThird->hasDataEntry('primary-1'))
            <p class="js-primary-line">{{ $lowerThird->getDataEntry('primary-1') }}</p>
        @endif

        @if($lowerThird->hasDataEntry('secondary-1'))
            <p class="js-secondary-line">{{ $lowerThird->getDataEntry('secondary-1') }}</p>
        @endif
    </div>
    <div class="person-2">
        @if($lowerThird->hasDataEntry('primary-2'))
            <p class="js-primary-line">{{ $lowerThird->getDataEntry('primary-2') }}</p>
        @endif

        @if($lowerThird->hasDataEntry('secondary-2'))
            <p class="js-secondary-line">{{ $lowerThird->getDataEntry('secondary-2') }}</p>
        @endif
    </div>
</section>
