<section class="lower-third_banner">
    @if($lowerThird->hasDataEntry('banner'))

        <img src="{{ asset('storage/lower-thirds/' . $lowerThird->getDataEntry('banner')) }}" alt="">

    @endif
</section>
