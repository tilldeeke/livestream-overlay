<section class="lower-third_single-person">
    @if($lowerThird->hasDataEntry('primary'))
        <p class="js-primary-line">{{ $lowerThird->getDataEntry('primary') }}</p>
    @endif

    @if($lowerThird->hasDataEntry('secondary'))
        <p class="js-secondary-line">{{ $lowerThird->getDataEntry('secondary') }}</p>
    @endif
</section>
