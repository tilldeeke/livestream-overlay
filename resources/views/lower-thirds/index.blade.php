@extends('layouts.app')

@section('content')

    <x-page-header title="Bauchbinden">
        <x-slot name="actions">
            <div class="btn-toolbar">
                <div class="btn-group mr-2" role="group" aria-label="First group">
                    <a class="btn btn-outline-secondary" href="{{ route('lower-thirds.display', ['preview' => true]) }}" target="_blank">
                        Preview <span class="fa fa-fw fa-external-link-alt"></span>
                    </a>
                </div>
                <div class="btn-group mr-2" role="group" aria-label="First group">
                    <a class="btn btn-secondary" href="{{ route('lower-thirds.display') }}" target="_blank">
                        Embed <span class="fa fa-fw fa-external-link-alt"></span>
                    </a>
                </div>
            </div>
        </x-slot>
    </x-page-header>

    <fieldset>
        <legend>Neue Bauchbinde</legend>
        @livewire('create-lower-third')
    </fieldset>

    @livewire('lower-third-list')

@endsection
