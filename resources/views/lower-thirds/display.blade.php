@extends('layouts.display')

@section('content')
    <section id="content"></section>
@endsection

@push('styles')
    <style>
        body {
            background: transparent;
            font-family: 'Source Sans Pro', sans-serif;
        }
        .hidden { display: none;}
    </style>
    @if(isset($_GET['preview']))
        <style>
            @php
        $gray1 = '#ccc';
        $gray2 = '#999'
        @endphp
        body {
                background: {{ $gray1 }};
                background-image:
                    linear-gradient(45deg, {{ $gray2 }} 25%, transparent 25%),
                    linear-gradient(-45deg, {{ $gray2 }} 25%, transparent 25%),
                    linear-gradient(45deg, transparent 75%, {{ $gray2 }} 75%),
                    linear-gradient(-45deg, transparent 75%, {{ $gray2 }} 75%)
            ;
                background-size: 20px 20px;
                background-position: 0 0, 0 10px, 10px -10px, -10px 0px;
            }
        </style>
    @endif
    <style>
        {!! \App\LowerThird\Styles\LowerThirdStyleRegistry::getAllCssBlocks(\App\Setting::get('lower_third_style')) !!}
    </style>
    <style>
        .cooperation {
            position: absolute;
            bottom: 50px;
            left: 50px;
            font-weight: bold;
        }
        .cooperation_title {
            display: inline-block;

            color:#fff;
            background: #A2257C;

            font-size: 24px;
            padding: 10px 15px;
            margin : -15px 0 0 10px;
        }
        .cooperation_logos {
            display: flex;
            flex-direction: row;
            align-items: center;

            background: white;
            padding: 24px;
        }
    </style>
    <style>
        .sponsors {
            position: absolute;
            bottom: 50px;
            left: 50px;
            font-weight: bold;
        }
        .sponsors_title {
            /*
            position: absolute;
            top: -40px;
            left: 10px;

             */
            display: inline-block;
            background:#A2257C;
            color: #fff;

            font-size: 24px;
            padding: 10px 15px;
            margin : -15px 0 0 10px;
        }
        .sponsors_logos {
            background: white;
            padding: 24px;

            display: flex;
            flex-direction: row;
        }
    </style>
@endpush
@push('scripts')
    <script>
        var content = document.querySelector('#content');
        Echo.channel('lower-third')
            .listen('.shown', (e) => {

                var entry = document.createElement('section');
                entry.classList.add('entry-' + e.lowerThird.id);
                entry.classList.add('animated');
                entry.classList.add(e.animationIn);

                entry.innerHTML = e.html;

                content.appendChild(entry);
            })
            .listen('.hidden', (e) => {
                var entries = document.querySelectorAll('.entry-' + e.lowerThird.id);

                if(entries) {
                    entries.forEach(function(entry) {
                        entry.classList.remove(e.animationIn);
                        entry.classList.add(e.animationOut);

                        entry.addEventListener("animationend", function(e) {
                            let target = e.target;

                            target.remove();
                        });
                    });
                }
            });
    </script>


@endpush
