@extends('layouts.display')

@section('content')
    <style>
        body {
            font-family: Arial, sans-serif;
        }
        /*
        .countdown-timer {
            border-bottom : 10px solid black;
            text-align: center;
            line-height: 0.8em;
            font-size: 35vw;
        }
         */
        .contdown_wrapper {
            display: flex;
            align-items: center;
            justify-content: center;
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }
        .countdown-timer {
            font-size: 40vw;
        }
        .scene-info {
            width: 100%;
            font-size: 12vh;
        }
        .scene-info td:first-child {
            width: 1%;
        }
    </style>
    <div class="contdown_wrapper">
        <div class="countdown-timer" id="js-countdown-time">--:--</div>
    </div>
    <!--
        <table class="scene-info">
            <tr>
                <td>LIVE:</td>
            <td><div id="js-countdown-scene-current">--</div></td>
            </tr>
            <tr>
                <td>Vorschau:</td>
                <td><div id="js-countdown-scene-preview">--</div></td>
            </tr>
        </table>
        -->
@endsection

@push('scripts')

    <script>
        // Scenes to watch
        var scenesToWatch = {
            @foreach($countdowns as $countdown)
            '{{ $countdown->scene->name }}': '{{ $countdown->duration_in_seconds }}',
            @endforeach
        };

        var displayTime = document.getElementById("js-countdown-time");
        var countdownDuration = 0;


        window.addEventListener("load", initialize, false);

        var intervalID = 0;
        var sceneRefreshInterval = 0;
        var socketisOpen = false;

        var jsTimeout;
        var intervalCounterUp;

        function initialize() {
            updateDisplay();
            connect();
        }

        function connect() {
            websocket = new WebSocket("ws://{{ $server_ip }}:{{$server_port}}");

            websocket.onopen = function(evt) {
                socketisOpen = 1;
                clearInterval(intervalID);
                intervalID = 0;
            };

            websocket.onclose = function(evt) {
                socketisOpen = 0;
                if (!intervalID) {
                    intervalID = setInterval(connectWebsocket, 5000);
                }
            };

            websocket.onmessage = function(evt) {
                var data = JSON.parse(evt.data);
                // console.log('onmessage', data);

                if (data.hasOwnProperty("update-type")) {
                    handleStateChangeEvent(data)
                } else {
                    console.log('onmessage unable to handle message.', data);
                }
            };

            websocket.onerror = function(evt) {
                socketisOpen = 0;
                if (!intervalID) {
                    intervalID = setInterval(connectWebsocket, 5000);
                }
            };
        }

        // set currentState values based on incoming websocket messages
        function handleStateChangeEvent(data) {
            // console.log("before update", currentState);
            // console.log(data);

            const updateType = data["update-type"];

            switch(updateType) {
                case "TransitionBegin":
                    clearCountdown();
                    startCountdownForScene(data["to-scene"]);

                    break;
            }
        }

        function clearCountdown() {
            window.clearInterval(intervalCounterUp);
            window.clearInterval(jsTimeout);
            countdownDuration = 0;
        }

        function startCountdownForScene(sceneName) {
            if (!scenesToWatch[sceneName]) {

                // Start a new up counter
                intervalCounterUp = window.setInterval(countup, 1000);
                countup();

                return;
            }

            countdownDuration = scenesToWatch[sceneName];

            // Start a new timer, for the specified duration
            jsTimeout = window.setInterval(countdown, 1000);
            countdown();
        }


        function countdown() {

            updateDisplay();

            countdownDuration--;

            if (countdownDuration <= 0) {
                countdownDuration = 0;
            }
        }

        function countup() {

            updateDisplay();

            countdownDuration++;

            if (countdownDuration <= 0) {
                countdownDuration = 0;
            }
        }

        function updateDisplay() {
            var minutes = parseInt(countdownDuration / 60, 10);
            var seconds = parseInt(countdownDuration % 60, 10);

            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;

            minutes = minutes < 0 ? '00' : minutes;
            seconds = seconds < 0 ? '00' : seconds;

            displayTime.textContent = minutes + ":" + seconds;
        }
    </script>
@endpush
