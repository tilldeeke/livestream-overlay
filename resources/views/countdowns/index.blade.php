@extends('layouts.app')

@section('content')
    <x-page-header title="Countdown" />

    <fieldset>
        <legend>Neuer Countdown</legend>
        <form action="{{ route('countdowns.store') }}" method="POST">
            @csrf
            <table>
                <tr>
                    <td><label for="scene_id">Szene:</label></td>
                    <td>
                        <select name="scene_id" id="scene_id">
                            @foreach($scenes as $scene)
                                <option value="{{ $scene->id }}" {{ (old('scene') == $scene->id)?'selected':'' }}>{{ $scene->name }}</option>
                            @endforeach
                        </select>
                        @error('scene')
                        <p>{{ $message }}</p>
                        @enderror
                    </td>
                </tr>
                <tr>
                    <td><label for="duration_in_seconds">Dauer [s]:</label></td>
                    <td>
                        <input type="number" name="duration_in_seconds" id="duration_in_seconds" value="{{ old('duration_in_seconds') }}">
                        @error('duration_in_seconds')
                        <p>{{ $message }}</p>
                        @enderror
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><button type="submit">Save</button></td>
                </tr>
            </table>
        </form>
    </fieldset>
    <table border="1">
        <thead>
        <tr>
            <th>Szene</th>
            <th>Dauer [s]</th>
        </tr>
        </thead>
        <tbody>
            @foreach($countdowns as $countdown)
                <tr>
                    <td>{{ $countdown->scene->name }}</td>
                    <td>{{ $countdown->duration_in_seconds }}</td>
                    <td>
                        <form action="{{ route('countdowns.destroy', $countdown) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit">X</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
