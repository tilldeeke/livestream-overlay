@extends('layouts.app')

@push('scripts')

    <script>
        var sceneRefreshInterval = 0;
        var socketisOpen = false;

        var scenes;

        function getCurrentScenes() {
            websocket = new WebSocket("ws://{{ \App\Setting::get('obs_websocket_server_ip', '127.0.0.1') }}:{{ \App\Setting::get('obs_websocket_server_port', '4444') }}");

            websocket.onopen = function(evt) {
                requestInitialState();
            };

            websocket.onmessage = function(evt) {
                var data = JSON.parse(evt.data);

                if (data.hasOwnProperty("message-id")) {
                    handleInitialStateEvent(data)
                } else {
                    console.log('onmessage unable to handle message.', data);
                }
            };

            websocket.onerror = function(evt) {
                alert('We could not connect to OBS! Please check the settings!');
            };
        }

        function requestInitialState() {
            // message-id: we make this up. used to identify response messages which are sent back from OBS.
            // request-type: command to send to OBS
            // @see https://github.com/Palakis/obs-websocket/blob/4.x-current/docs/generated/protocol.md#getscenelist
            websocket.send(JSON.stringify({
                "message-id": "get-scene-list",
                "request-type": "GetSceneList"
            }));
        }

        // process responses to requests sent by requestInitialState
        function handleInitialStateEvent(data) {
            const messageId = data["message-id"];

            switch(messageId) {
                case "get-scene-list":
                    scenes = data['scenes'];
                    break;
                default:
                    console.error('handleInitialStateEvent got unknown event.', data);
            }

            updateTextarea();
        }
        function updateTextarea() {
            content = '';
            for(i =0; i < scenes.length; i++) {
                scene = scenes[i];

                content = content + scene.name + "\n";
            }

            document.getElementById('scenes').textContent = content;
        }
    </script>
@endpush



@section('content')
    <x-page-header title="Szenen" />

    <fieldset>
        <legend>Szenen synchronisieren</legend>
        <form action="{{ route('scenes.store') }}" method="POST" class="form-horizontal">
            @csrf
            <div class="form-group row">
                <label class="col-sm-2 col-form-label"for="synchronize">Synchronisieren:</label>
                <div class="col-sm-10">
                    <button class="btn btn-secondary" id="synchronize" type="button" onclick="getCurrentScenes()">Szenen aus OBS übernehmen</button>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label"for="scenes">Szenen:</label>
                <div class="col-sm-10">
                    <textarea name="scenes" id="scenes" cols="30" rows="10" class="form-control @error('scenes') is-invalid @enderror"></textarea>
                    @error('scenes')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-10 offset-sm-2">
                    <button type="submit" class="btn btn-primary">Speichern</button>
                </div>
            </div>

        </form>
    </fieldset>
    <h3>Szenen</h3>
    <table class="table table-bordered table-striped table-sm">
        <thead>
        <tr>
            <th>Szene</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
            @foreach($scenes as $scene)
                <tr>
                    <td>{{ $scene->name }}</td>
                    <td>
                        <form action="{{ route('scenes.destroy', $scene) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-sm btn-danger">Löschen <span class="fa fa-trash"></span></button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
