<div>
    <form action="{{ route('lower-thirds.store') }}" method="post" wire:submit.prevent="submit">
        @csrf
        <fieldset wire:loading.attr="disabled" wire:target="style">
        <div class="form-group row">
            <label for="style" class="col-sm-2 col-form-label">Stil:</label>
            <div class="col-sm-10">
                <select name="style" id="style" wire:model="style" class="form-control @error('style') is-invalid @enderror">
                    <option value="" selected>[Stil auswählen]</option>

                    @foreach($availableStyles as $key => $label)
                        <option value="{{ $key }}">{{ $label }}</option>
                    @endforeach
                </select>
                @error('style')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
        </div>


            @if($style)
                <fieldset>
                    <legend>Fields</legend>
                @foreach($this->fields as $field)

                    <div class="form-group row">
                        <label for="entry-{{ $field->getKey() }}" class="col-sm-2 col-form-label">{{ $field->getLabel() }}{{ $field->isRequired()?'*':'' }}:</label>
                        <div class="col-sm-10">
                            @if($field instanceof \App\LowerThird\Fields\TextField)
                            <input
                                type="text"
                                name="entry[{{ $field->getKey() }}]"
                                id="entry-{{ $field->getKey() }}"
                                wire:model="entry.{{ $field->getKey() }}"
                                class="form-control @error('entry.' . $field->getKey()) is-invalid @enderror"
                                @if($field->getInstructions())
                                    aria-describedby="entry-{{ $field->getKey() }}-instructions"
                                @endif
                            >
                            @endif
                            @if($field instanceof \App\LowerThird\Fields\ImageField)
                                <input
                                    type="file"
                                    name="entry[{{ $field->getKey() }}]"
                                    id="entry-{{ $field->getKey() }}"
                                    wire:model="entry.{{ $field->getKey() }}"
                                    class="form-control @error('entry.' . $field->getKey()) is-invalid @enderror"
                                    @if($field->getInstructions())
                                        aria-describedby="entry-{{ $field->getKey() }}-instructions"
                                    @endif
                                >
                            @endif

                            @if($field->getInstructions())
                            <small id="entry-{{ $field->getKey() }}-instructions" class="form-text text-muted">
                                {{ $field->getInstructions() }}
                            </small>
                            @endif

                            @error('entry.' . $field->getKey())
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                @endforeach
                </fieldset>
            @endif

        <div class="form-group row">
            <div class="col-sm-10 offset-sm-2">
                <button type="submit" class="btn btn-primary">Speichern</button>
            </div>
        </div>
        </fieldset>
    </form>
</div>
