<div>
    <fieldset>
        <legend>Filter</legend>
        <table class="table table-bordered table-sm">
            <thead>
            <tr>
                <th>Suche</th>
                <th>Stil</th>
                <th>Status</th>
            </tr>
            <tr>
                <td><input type="text" wire:model="search" class="form-control"></td>
                <td>
                    <select name="style" id="style" wire:model="style" class="form-control">
                        <option value="">ALL</option>
                        @foreach(\App\LowerThird\Styles\LowerThirdStyleRegistry::getNames() as $key => $label)
                            <option value="{{ $key }}">{{ $label }}</option>
                        @endforeach
                    </select>
                </td>
                <td>
                    <select name="status" id="status" wire:model="status" class="form-control">
                        <option value="">ALL</option>
                        @foreach(\App\Enums\LowerThirdStatus::getAll() as $status)
                            <option value="{{ $status->getValue() }}">{{ $status->getName() }}</option>
                        @endforeach
                    </select>
                </td>
            </tr>
            </thead>
        </table>
    </fieldset>

    <fieldset>
        <legend>Lower Thirds</legend>
        <table class="table table-bordered table-sm">
            <thead>
            <tr>
                <th>
                        Text
                </th>
                <th>
                        Stil
                </th>
                <th>
                        Aktionen
                </th>
            </tr>
            </thead>
            @foreach($entries as $entry)
                <tr wire:key="{{ $entry->id }}">
                    <td>
                        <dl>
                        @foreach(json_decode($entry->entry_data) as $key => $value)
                            <dt>{{ $key }}</dt>
                            <dd>{{ $value }}</dd>
                        @endforeach
                        </dl>
                    </td>
                    <td>{{ ($entry->style)?$entry->style::getLabel():'UNKNOWN' }}</td>
                    <td>
                        <div class="btn-toolbar">
                            <div class="btn-group mr-2" role="group" aria-label="Toggle visibility">
                                @livewire('toggle-lower-third-visibility', ['lowerThird' => $entry], key('status-' . $entry->id))
                            </div>
                            <div class="btn-group mr-2" role="group" aria-label="Delete lower third">
                                <div class="dropdown">
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="fa fa-fw fa-cog"></span>
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <div wire:loading wire:target="delete">
                                                <button type="button" class="btn-link" wire:loading><span class="fa fa-sync"></span>Loading</button>
                                            </div>
                                            <div wire:loading.remove wire:target="delete">
                                                <button type="button" class="dropdown-item d-flex justify-content-between align-items-center" wire:click="delete({{ $entry->id }})">
                                                    <span>Delete</span>
                                                    <span class="fa fa-fw fa-trash"></span>
                                                </button>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
        </table>
    </fieldset>
</div>
