<div>
    <table>
        <tbody>
            @foreach($messages as $message)
                <tr>
                    <td>{{ $message->source }}</td>
                    <td>{{ $message->author_name }}</td>
                    <td><img src="{{ $message->author_avatar }}" alt="" height="50"></td>
                    <td>{{ $message->message }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
