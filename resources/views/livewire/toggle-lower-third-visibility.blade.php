<div>
    <div wire:loading>
        <button type="button" class="btn btn-secondary" wire:loading><span class="fa fa-sync"></span>Loading</button>
    </div>
    <div wire:loading.remove>
        @if($lowerThird->status == \App\Enums\LowerThirdStatus::HIDDEN())
            <button type="button" class="btn btn-danger" wire:click="show"><span class="fa fa-fw fa-eye mr-2"></span>Show</button>
        @elseif($lowerThird->status == \App\Enums\LowerThirdStatus::ACTIVE())
            <button type="button" class="btn btn-success" wire:click="hide"><span class="fa fa-fw fa-eye-slash mr-2"></span>Hide</button>
        @endif
    </div>
</div>
