<div class="border-bottom  mb-5">
    <section class="d-flex justify-content-between mb-2">
        <h3>
            {{ $title }}
            @if($subtitle)
                <small class="text-muted">{{ $subtitle }}</small>
            @endif
        </h3>
        <div class="mb-2">
            @if($actions)
                {{ $actions }}
            @endif
        </div>
    </section>
    @if($breadcrumbs)
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                @foreach($breadcrumbs as $name => $url)
                    <li class="breadcrumb-item"><a href="{{ $url }}">{{ $name }}</a></li>
                @endforeach
                <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
            </ol>
        </nav>
    @endif
</div>
