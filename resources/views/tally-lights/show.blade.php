@extends('layouts.display')

@section('content')
    <style>
        html, body {
            height: 100%;
        }
        body {
            display: flex;
            align-items: center;
            justify-content: center;

            background: green;
        }
        .scene-current {
            background: red;
        }
        .scene-preview {
            background: orange;
        }
        .scene-names {
            font-family: Arial, sans-serif;
            color: white;
            text-align: center;
            font-size: 15vh;
        }
    </style>
    <div class="scene-names">
        {{ $tallyLight->scenes->implode('name', ', ') }}
    </div>
@endsection

@push('scripts')

    <script>
        // Scenes to watch
        var scenesToWatch = [
            @foreach($tallyLight->scenes as $scene)
            '{{ $scene->name }}',
            @endforeach
        ];

        var currentState = {
            "currentScene" : "",
            "previewScene" : "",
        };

        window.addEventListener("load", initialize, false);

        var intervalID = 0;
        var sceneRefreshInterval = 0;
        var socketisOpen = false;

        function initialize() {
            connect();
        }

        function connect() {
            websocket = new WebSocket("ws://{{ \App\Setting::get('obs_websocket_server_ip', '127.0.0.1') }}:{{ \App\Setting::get('obs_websocket_server_port', '4444') }}");

            websocket.onopen = function(evt) {
                socketisOpen = 1;
                clearInterval(intervalID);
                intervalID = 0;
                requestInitialState();
            };

            websocket.onclose = function(evt) {
                socketisOpen = 0;
                if (!intervalID) {
                    intervalID = setInterval(connectWebsocket, 5000);
                }
            };

            websocket.onmessage = function(evt) {
                var data = JSON.parse(evt.data);
                // console.log('onmessage', data);

                if (data.hasOwnProperty("message-id")) {
                    handleInitialStateEvent(data)
                } else if (data.hasOwnProperty("update-type")) {
                    handleStateChangeEvent(data)
                } else {
                    console.log('onmessage unable to handle message.', data);
                }
            };

            websocket.onerror = function(evt) {
                socketisOpen = 0;
                if (!intervalID) {
                    intervalID = setInterval(connectWebsocket, 5000);
                }
            };
        }

        function requestInitialState() {
            // message-id: we make this up. used to identify response messages which are sent back from OBS.
            // request-type: command to send to OBS
            const commands = [
                {
                    "message-id": "get-current-scene",
                    // https://github.com/Palakis/obs-websocket/blob/4.x-current/docs/generated/protocol.md#getcurrentscene
                    "request-type": "GetCurrentScene"
                },
                {
                    "message-id": "get-preview-scene",
                    // https://github.com/Palakis/obs-websocket/blob/4.x-current/docs/generated/protocol.md#getpreviewscene
                    "request-type": "GetPreviewScene"
                }
            ];

            for (let i = 0; i < commands.length; i++) {
                let payload = commands[i];

                // console.log('sending command', payload);
                if (socketisOpen) {
                    websocket.send(JSON.stringify(payload));
                } else {
                    console.error('unable to send command. socket not open.', payload);
                }
            }
        }

        // process responses to requests sent by requestInitialState
        function handleInitialStateEvent(data) {
            const messageId = data["message-id"];

            switch(messageId) {
                case "get-current-scene":
                    currentState.currentScene = data['name'];
                    break;
                case "get-preview-scene":
                    currentState.previewScene = data['name'];
                    break;
                default:
                    console.error('handleInitialStateEvent got unknown event.', data);
            }

            updateDisplay();
        }


        // set currentState values based on incoming websocket messages
        function handleStateChangeEvent(data) {
            // console.log("before update", currentState);
            // console.log(data);

            const updateType = data["update-type"];

            switch(updateType) {
                case "PreviewSceneChanged":
                    currentState.previewScene = data["scene-name"];
                    updateDisplay();
                    break;
                case "SwitchScenes":
                    currentState.currentScene = data["scene-name"];
                    updateDisplay();
                    break;
                case "TransitionBegin":
                    currentState.currentScene = data["to-scene"];
                    updateDisplay();
                    break;
            }
        }

        function updateDisplay() {

            document.body.classList.remove('scene-current');
            document.body.classList.remove('scene-preview');

            if (scenesToWatch.includes(currentState.currentScene)) {
                document.body.classList.add('scene-current');
            } else if (scenesToWatch.includes(currentState.previewScene)) {
                // document.body.classList.add('scene-preview');
            }

        }
    </script>
@endpush
