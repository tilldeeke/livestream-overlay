@extends('layouts.app')

@section('content')
    <x-page-header title="Tally Lights" />

    <fieldset>
        <legend>Neues Tally Light</legend>
        <form action="{{ route('tally-lights.store') }}" method="POST">
            @csrf
            <table>
                <tr>
                    <td><label for="name">Name:</label></td>
                    <td>
                        <input type="text" name="name" id="name" value="{{ old('name') }}">
                        @error('name')
                            <p>{{ $message }}</p>
                        @enderror
                    </td>
                </tr>
                <tr>
                    <td><label for="scenes">Szenen:</label></td>
                    <td>
                        <select name="scenes[]" id="scenes" multiple>
                            @foreach($scenes as $scene)
                                <option value="{{ $scene->id }}">{{ $scene->name }}</option>
                            @endforeach
                        </select>
                        @error('scenes')
                            <p>{{ $message }}</p>
                        @enderror
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><button type="submit">Save</button></td>
                </tr>
            </table>
        </form>
    </fieldset>
    <table border="1">
        <thead>
        <tr>
            <th>Name</th>
            <th>Szenen</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
            @foreach($tallyLights as $tallyLight)
                <tr>
                    <td><a href="{{ route('tally-lights.show', $tallyLight) }}">{{ $tallyLight->name }}</a></td>
                    <td>{{ $tallyLight->scenes->implode('name', ', ') }}</td>
                    <td>
                        <form action="{{ route('tally-lights.destroy', $tallyLight) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit">X</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
