@extends('layouts.app')

@section('content')

    <x-page-header title="Einstellungen" />

    @php
    use App\Setting;
    @endphp
    <form action="{{ route('settings.general.update') }}" method="POST">
        @method('PUT')
        @csrf

        <section class="row border-bottom pb-4 mb-4">
            <div class="col-4">
                <h4 class="h5">OBS</h4>
                <p class="text-muted">Die Serverinformationen zum Websocket-Server, der in OBS installiert ist.</p>
            </div>
            <div class="col-8">
                <div class="form-row">
                    <div class="form-group col-md-8">
                        <label for="obs_websocket_server_ip">IP</label>
                        <input type="text" class="form-control @error('obs_websocket_server_ip') is-invalid @enderror" name="obs_websocket_server_ip" id="obs_websocket_server_ip" value="{{ old('obs_websocket_server_ip', Setting::get('obs_websocket_server_ip')) }}">
                        @error('obs_websocket_server_ip')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group col-md-4">
                        <label for="obs_websocket_server_port">Port</label>
                        <input
                            type="text"
                            class="form-control @error('obs_websocket_server_port') is-invalid @enderror"
                            name="obs_websocket_server_port"
                            id="obs_websocket_server_port"
                            value="{{ old('obs_websocket_server_port', Setting::get('obs_websocket_server_port')) }}"
                            aria-describedby="obs_websocket_server_port_help"
                        >
                        @error('obs_websocket_server_port')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                        <small id="obs_websocket_server_port_help" class="form-text text-muted">
                            Standard: 4444
                        </small>
                    </div>
                </div>
                <div class="form-row">
                    <button class="btn btn-primary">Speichern</button>
                </div>
            </div>
        </section>

        <section class="row pb-4 mb-4">
            <div class="col-4">
                <h4 class="h5">Bauchbinden</h4>
                <p class="text-muted">Lege Animationen für die Bauchbinden fest.</p>
            </div>
            <div class="col-8">
                <div class="form-group">
                    <label for="lower_third_animation_in">Einblendung</label>
                    <input type="text" class="form-control @error('lower_third_animation_in') is-invalid @enderror" name="lower_third_animation_in" id="lower_third_animation_in" value="{{ old('lower_third_animation_in', Setting::get('lower_third_animation_in')) }}">
                    @error('lower_third_animation_in')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="lower_third_animation_out">Ausblendung</label>
                    <input type="text" class="form-control @error('lower_third_animation_out') is-invalid @enderror" name="lower_third_animation_out" id="lower_third_animation_out" value="{{ old('lower_third_animation_out', Setting::get('lower_third_animation_out')) }}">
                    @error('lower_third_animation_out')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="lower_third_style">Style</label>
                    <select name="lower_third_style" id="lower_third_style" class="form-control @error('lower_third_animation_out') is-invalid @enderror">
                        <?php $options = ['hannovercsd' => 'Hannover.CSD', 'andersraum' => 'Andersraum e.V.']; ?>
                        @foreach($options as $key => $value)
                            <option value="{{ $key }}" {{ (old('lower_third_style', Setting::get('lower_third_style')) == $key)?'selected':'' }}>{{ $value }}</option>
                        @endforeach
                    </select>
                    @error('lower_third_style')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-row">
                    <button class="btn btn-primary">Speichern</button>
                </div>
            </div>
        </section>
    </form>

@endsection
