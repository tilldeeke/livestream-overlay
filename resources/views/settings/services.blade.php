@extends('layouts.app')

@section('content')

    <x-page-header title="Dienste" :breadcrumbs="['Einstellungen' => route('settings.general')]" />

    <section class="row mb-4">
        <div class="col-4">
            <h4 class="h5">Restream.io</h4>
        </div>
        <div class="col-8">
            @if($restream->isAuthenticated())
                <p class="text-muted">Verbunden als <strong>{{ $restream->getUser()->getEmail() }}</strong></p>
                <form action="{{ route('settings.services.restream.disconnect') }}" method="POST">
                    @csrf
                    <button type="submit" class="btn btn-outline-danger">Verbindung trennen</button>
                </form>
            @else
                <a href="{{ route('settings.services.restream.connect') }}" class="btn btn-primary">Verbinden</a>
            @endif
        </div>
    </section>


@endsection
