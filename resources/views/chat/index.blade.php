@extends('layouts.app')

@section('content')
    <x-page-header title="Chat" />

    @livewire('chat-message-list')
@endsection
