<?php

use App\Http\Controllers\ChatMessageController;
use App\Http\Controllers\CountdownController;
use App\Http\Controllers\CountdownDisplay;
use App\Http\Controllers\DisplayController;
use App\Http\Controllers\GlideImageController;
use App\Http\Controllers\LowerThirdController;
use App\Http\Controllers\OverviewController;
use App\Http\Controllers\SceneController;
use App\Http\Controllers\Settings\GeneralSettingsController;
use App\Http\Controllers\Settings\Services\RestreamIOController;
use App\Http\Controllers\Settings\ServiceSettingsController;
use App\Http\Controllers\TallyLightController;
use App\Http\Controllers\TallyLightDisplayController;
use Illuminate\Support\Facades\Route;

/**
 * Overview
 */
Route::get('/', OverviewController::class)->name('overview.index');

/**
 * Chat Messages
 */
Route::get('chat', [ChatMessageController::class, 'index'])->name('chat.index');

/**
 * Glide
 */
Route::get('glide/{path}', GlideImageController::class)
    ->where('path', '.*')
    ->name('glide');

/**
 * Settings
 */
Route::get('settings', [GeneralSettingsController::class, 'index'])->name('settings.general');
Route::put('settings', [GeneralSettingsController::class, 'update'])->name('settings.general.update');

Route::get('settings/services', ServiceSettingsController::class)->name('settings.services');

/*
 * Restream.io
 */
Route::get('settings/services/restream.io/connect', [RestreamIOController::class, 'connect'])->name('settings.services.restream.connect');
Route::get('settings/services/restream.io/callback', [RestreamIOController::class, 'callback'])->name('settings.services.restream.callback');
Route::post('settings/services/restream.io/disconnect', [RestreamIOController::class, 'disconnect'])->name('settings.services.restream.disconnect');

/**
 * Scene Preview
 */
Route::view('scene-status', 'scene-status')->name('scene-status.display');

/**
 * Lower Thirds
 */

Route::get('lower-thirds/display', [LowerThirdController::class, 'display'])->name('lower-thirds.display');
Route::get('lower-thirds', [LowerThirdController::class, 'index'])->name('lower-thirds.index');
Route::post('lower-thirds', [LowerThirdController::class, 'store'])->name('lower-thirds.store');
Route::get('lower-thirds/{lowerThird}', [LowerThirdController::class, 'index'])->name('lower-thirds.show');
Route::delete('lower-thirds/{lowerThird}', [LowerThirdController::class, 'destroy'])->name('lower-thirds.destroy');

Route::post('lower-thirds/{lowerThird}/show', [LowerThirdController::class, 'show'])->name('lower-thirds.action.show');
Route::post('lower-thirds/{lowerThird}/hide', [LowerThirdController::class, 'hide'])->name('lower-thirds.action.hide');

/**
 * Displays
 */
Route::get('displays', [DisplayController::class, 'index'])->name('displays.index');
Route::post('displays', [DisplayController::class, 'store'])->name('displays.store');
Route::get('displays/{display}', [DisplayController::class, 'show'])->name('displays.show');
Route::delete('displays/{display}', [DisplayController::class, 'destroy'])->name('displays.destroy');

/**
 * Scenes
 */
Route::get('scenes', [SceneController::class, 'index'])->name('scenes.index');
Route::post('scenes', [SceneController::class, 'store'])->name('scenes.store');
Route::delete('scenes/{scene}', [SceneController::class, 'destroy'])->name('scenes.destroy');

/**
 * Tally Lights
 */
Route::get('tally-lights/display', TallyLightDisplayController::class)->name('tally-lights.display');
Route::get('tally-lights', [TallyLightController::class, 'index'])->name('tally-lights.index');
Route::post('tally-lights', [TallyLightController::class, 'store'])->name('tally-lights.store');
Route::get('tally-lights/{tallyLight}', [TallyLightController::class, 'show'])->name('tally-lights.show');
Route::delete('tally-lights/{tallyLight}', [TallyLightController::class, 'destroy'])->name('tally-lights.destroy');

/**
 * Countdowns
 */
Route::get('countdowns/display', CountdownDisplay::class)->name('countdowns.display');
Route::get('countdowns', [CountdownController::class, 'index'])->name('countdowns.index');
Route::post('countdowns', [CountdownController::class, 'store'])->name('countdowns.store');
Route::delete('countdowns/{countdown}', [CountdownController::class, 'destroy'])->name('countdowns.destroy');
