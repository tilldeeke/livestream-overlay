#!/bin/sh

# Generate required directories in storage
mkdir -p /var/www/html/storage/framework/cache/
mkdir -p /var/www/html/storage/framework/sessions/
mkdir -p /var/www/html/storage/framework/views/
mkdir -p /var/www/html/storage/app/
mkdir -p /var/www/html/storage/app/public

# Link the public storage directory
php artisan storage:link

# Check if a database file exists, create it otherwise
touch -a /var/www/html/storage/livestream-overlay.db

# Optimize laravel
php artisan optimize
php artisan config:cache
php artisan route:cache
php artisan view:cache

# Migrate the database
cd /var/www/html/ || exit
php artisan migrate --force --no-interaction --no-ansi
