<?php

namespace App;

use App\Events\ChatMessageCreated;
use App\Services\Restream\Chat\ChatMessageSource;
use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;

class ChatMessage extends Model
{
    protected static $unguarded = true;

    protected $dispatchesEvents = [
        'created' => ChatMessageCreated::class,
    ];

    /**
     *  Setup model event hooks
     */
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string) Uuid::uuid4();
        });
    }

    public function getSourceAttribute() : ChatMessageSource {

        if ($this->attributes['source'] instanceof ChatMessageSource) {
            return $this->attributes['source'];
        }

        return ChatMessageSource::make($this->attributes['source']);
    }

    public function setSourceAttribute(ChatMessageSource $value) : void {
        $this->attributes['source'] = $value->getValue();
    }
}
