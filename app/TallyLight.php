<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class TallyLight extends Model
{
    protected $guarded = [];

    public function scenes() : BelongsToMany {
        return $this->belongsToMany(Scene::class, 'tally_light_scenes');
    }
}
