<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Countdown extends Model
{
    protected $guarded = [];

    public function scene() : BelongsTo {
        return $this->belongsTo(Scene::class);
    }
}
