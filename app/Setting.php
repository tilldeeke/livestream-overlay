<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $guarded = [];

    public static function set(string $key, $value) : void {
        self::updateOrCreate(['setting_key' => $key], [
            'setting_value' => serialize($value)
        ]);
    }

    public static function get(string $key, $fallback = null) {
        $setting = self::firstWhere('setting_key', $key);

        if (!$setting) {
            return $fallback;
        }

        return unserialize($setting->setting_value);
    }
}
