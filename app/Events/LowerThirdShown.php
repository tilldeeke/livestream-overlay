<?php

namespace App\Events;

use App\LowerThird;
use App\Setting;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class LowerThirdShown implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var LowerThird
     */
    public $lowerThird;

    /**
     * @var string
     */
    public $animationIn;
    /**
     * @var string
     */
    public $animationOut;

    /**
     * @var array|string
     */
    public $html;

    /**
     * Create a new event instance.
     *
     * @param LowerThird $lowerThird
     */
    public function __construct(LowerThird $lowerThird)
    {
        $this->lowerThird = $lowerThird;

        $this->animationIn = Setting::get('lower_third_animation_in', 'fadeIn');
        $this->animationOut = Setting::get('lower_third_animation_out', 'fadeOut');

        $this->html = $lowerThird->getStyleAttribute()->getHtml($lowerThird);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return [
            new Channel('lower-third'),
            new Channel('lower-third.' . $this->lowerThird->id)
        ];
    }

    public function broadcastAs() {
        return 'shown';
    }
}
