<?php

namespace App\Events;

use App\LowerThird;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class LowerThirdCreated implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    /**
     * @var LowerThird
     */
    private $lowerThird;

    /**
     * Create a new event instance.
     *
     * @param LowerThird $lowerThird
     */
    public function __construct(LowerThird $lowerThird)
    {
        $this->lowerThird = $lowerThird;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return [
            new Channel('lower-third'),
            new Channel('lower-third.' . $this->lowerThird->id)
        ];
    }

    public function broadcastAs() {
        return 'created';
    }
}
