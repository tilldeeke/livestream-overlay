<?php

namespace App\View\Components;

use Illuminate\View\Component;

class PageHeader extends Component
{
    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $subtitle;

    /**
     * @var array
     */
    public $breadcrumbs;

    public $actions;


    /**
     * Create a new component instance.
     *
     * @param string $title
     * @param string $subtitle
     * @param array $breadcrumbs
     */
    public function __construct(string $title, string $subtitle = '', $breadcrumbs = [])
    {
        $this->title = $title;
        $this->subtitle = $subtitle;
        $this->breadcrumbs = $breadcrumbs;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.page-header');
    }
}
