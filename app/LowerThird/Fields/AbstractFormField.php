<?php


namespace App\LowerThird\Fields;


abstract class AbstractFormField
{
    protected string $key;
    protected string $label = '';
    protected string $instructions = '';
    protected bool $isRequired = false;
    protected array $rules = [];

    /**
     * AbstractFormField constructor.
     * @param string $key
     */
    public function __construct(string $key)
    {
        $this->key = $key;
    }

    public static function make(string $key) {
        return new static($key);
    }

    /**
     * @param string $label
     */
    public function setLabel(string $label): self
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return string
     */
    public function getInstructions(): string
    {
        return $this->instructions;
    }

    /**
     * @param string $instructions
     * @return AbstractFormField
     */
    public function setInstructions(string $instructions): self
    {
        $this->instructions = $instructions;
        return $this;
    }

    /**
     * @param bool $isRequired
     * @return AbstractFormField
     */
    public function setRequired(bool $isRequired): self
    {
        $this->isRequired = $isRequired;
        return $this;
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @return bool
     */
    public function isRequired(): bool
    {
        return $this->isRequired;
    }

    /**
     * @return array
     */
    public function getRules(): array
    {
        return $this->rules;
    }

    /**
     * @param array $rules
     * @return AbstractFormField
     */
    public function setRules(array $rules): self
    {
        $this->rules = $rules;
        return $this;
    }
}
