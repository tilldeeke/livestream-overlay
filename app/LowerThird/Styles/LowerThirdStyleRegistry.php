<?php


namespace App\LowerThird\Styles;


use App\LowerThird\Styles\AbstractLowerThirdStyle;
use App\LowerThird\Styles\BannerLowerThird;

class LowerThirdStyleRegistry
{
    protected static $styles = [
        SinglePersonLowerThird::class,
        TwoPeopleLowerThird::class,
        BannerLowerThird::class
    ];

    public static function getNames() : array {
        return collect(self::$styles)
            ->mapWithKeys(function(string $className) {
                return [forward_static_call([$className, 'getKey']) => forward_static_call([$className, 'getLabel'])];
            })
            ->all();
    }

    public static function getByName(string $styleName) : ?AbstractLowerThirdStyle
    {
        foreach(self::$styles as $className) {
            if (forward_static_call([$className, 'getKey']) === $styleName) {
                return new $className;
            }
        }

        return null;
    }

    public static function getAllCssBlocks(string $style = null) : string
    {
        return collect(self::$styles)
            ->map(function(string $className) use ($style) {
                return forward_static_call([$className, 'getCss'], $style);
            })
            ->join('');
    }
}
