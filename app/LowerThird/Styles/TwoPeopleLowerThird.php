<?php


namespace App\LowerThird\Styles;

use App\LowerThird;
use App\LowerThird\Fields\TextField;
use App\LowerThird\Styles\AbstractLowerThirdStyle;
use Ramsey\Uuid\Uuid;

class TwoPeopleLowerThird extends AbstractLowerThirdStyle
{
    static public function getKey(): string
    {
        return 'two-people';
    }

    static public function getLabel(): string
    {
        return 'Two people';
    }

    /**
     * @inheritDoc
     */
    public function getFields(): array
    {
        return [
            TextField::make('primary-1')
                ->setLabel('Primär 1')
                ->setRequired(true)
                ->setRules(['required', 'min:2']),
            TextField::make('secondary-1')
                ->setLabel('Sekundär 1')
                ->setRules(['min:2']),
            TextField::make('primary-2')
                ->setLabel('Primär 2')
                ->setRequired(true)
                ->setRules(['required', 'min:2']),
            TextField::make('secondary-2')
                ->setLabel('Sekundär 2')
                ->setRules(['min:2']),
        ];
    }

    public function getHtml(LowerThird $lowerThird) : string {
        return view('lower-thirds.styles.two-people', [
            'lowerThird' => $lowerThird
        ])->render();
    }

    static function getBaseCss() : string {
        return <<<CSS
        .lower-third_two-people .person-1 {
            position: absolute;
            bottom: 50px;
            left: 50px;
            font-weight: bold;
        }

        .lower-third_two-people .person-2 {
            position: absolute;
            bottom: 50px;
            right: 50px;
            font-weight: bold;
        }

        .lower-third_two-people .js-primary-line,
        .lower-third_two-people .js-secondary-line {
            display: inline-block;

        }

        .lower-third_two-people .person-1 .js-primary-line,
        .lower-third_two-people .person-1 .js-secondary-line {
            float: left
        }

        .lower-third_two-people .person-2 .js-primary-line,
        .lower-third_two-people .person-2 .js-secondary-line {
            float: right
        }

        .lower-third_two-people .js-primary-line {
            font-size: 48px;
            padding: 24px;
        }
        .lower-third_two-people .js-secondary-line {
            font-size: 24px;
            padding: 10px 15px;
            margin : -15px 0 0 0;
        }

        .lower-third_two-people .person-1 .js-secondary-line {
            margin-left: 10px;
        }
        .lower-third_two-people .person-2 .js-secondary-line {
            margin-right: 10px;
        }
CSS;
    }

    static public function getCss(string $style = null) : string {
        $css = self::getBaseCss();

        if ($style == 'andersraum') {
            $css .= <<<CSS
                @import url('https://fonts.googleapis.com/css2?family=Play&display=swap');

                .lower-third_two-people {
                    font-family: 'Play', sans-serif;
                }

                .lower-third_two-people .js-primary-line {
                    background:#ec6708;
                    color: #fff;
                }

                .lower-third_two-people .js-secondary-line {
                    color:#333;
                    background: #fff;
                }
            CSS;
        }

        if ($style == 'hannovercsd') {
            $css .= <<<CSS
                .lower-third_two-people .js-primary-line {
                    background:#A2257C;
                    color: #fff;
                }
                .lower-third_two-people .js-secondary-line {
                    color:#A2257C;
                    background: #fff;
                }
            CSS;
        }

        return $css;
    }


}
