<?php


namespace App\LowerThird\Styles;

use App\LowerThird\Fields\TextField;
use App\LowerThird\Styles\AbstractLowerThirdStyle;
use App\LowerThird\Fields\ImageField;
use App\LowerThird;
use Illuminate\Validation\Rule;
use Ramsey\Uuid\Uuid;

class BannerLowerThird extends AbstractLowerThirdStyle
{
    static public function getKey(): string
    {
        return 'banner';
    }

    static public function getLabel(): string
    {
        return 'Banner';
    }

    /**
     * @inheritDoc
     */
    public function getFields(): array
    {
        return [
            TextField::make('title')
                ->setLabel('Titel')
                ->setRules(['string', 'min:2']),
            ImageField::make('banner')
                ->setLabel('Banner')
                ->setInstructions('Muss min 200px * 800px groß sein')
                ->setRequired(true)
                ->setRules([
                    'required',
                    'image',
                    Rule::dimensions()->minHeight(200)->minWidth(800)
                ])
        ];
    }

    public function processValidatedData(array $data) {
        // Store the banner
        $extension = $data['banner']->guessExtension();
        $filename = Uuid::uuid4() . '.' . $extension;

        $image = $data['banner']->storeAs('public/lower-thirds', $filename);

        $data['banner'] = $filename;

        return $data;
    }

    public function getHtml(LowerThird $lowerThird) : string {
        return view('lower-thirds.styles.banner', [
            'lowerThird' => $lowerThird
        ])->render();
    }

}
