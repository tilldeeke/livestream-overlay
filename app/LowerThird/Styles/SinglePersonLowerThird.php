<?php


namespace App\LowerThird\Styles;

use App\LowerThird;
use App\LowerThird\Styles\AbstractLowerThirdStyle;
use App\LowerThird\Fields\TextField;
use Ramsey\Uuid\Uuid;

class SinglePersonLowerThird extends AbstractLowerThirdStyle
{
    static public function getKey(): string
    {
        return 'single-person';
    }

    static public function getLabel(): string
    {
        return 'Single Person';
    }

    /**
     * @inheritDoc
     */
    public function getFields(): array
    {
        return [
            TextField::make('primary')
                ->setLabel('Primär')
                ->setRequired(true)
                ->setRules(['required', 'min:2']),
            TextField::make('secondary')
                ->setLabel('Sekundär')
                ->setRules(['min:2']),
        ];
    }

    public function getHtml(LowerThird $lowerThird) : string {
        return view('lower-thirds.styles.two-lines', [
            'lowerThird' => $lowerThird
        ])->render();
    }

    static function getBaseCss() : string {
        return <<<CSS
            .lower-third_single-person {
                position: absolute;
                bottom: 50px;
                left: 50px;
                font-weight: bold;
            }

            .lower-third_single-person .js-primary-line, .js-secondary-line {
                display: inline-block;
                float: left;
                clear: both;
            }

            .lower-third_single-person .js-primary-line {
                font-size: 48px;
                padding: 24px;
            }
            .lower-third_single-person .js-secondary-line {
                font-size: 24px;
                padding: 10px 15px;
                margin : -15px 0 0 10px;
            }
        CSS;
    }

    static public function getCss(string $style = null) : string
    {
        $css = self::getBaseCss();

        if ($style == 'andersraum') {
            $css .= <<<CSS
                @import url('https://fonts.googleapis.com/css2?family=Play&display=swap');

                .lower-third_single-person {
                    font-family: 'Play', sans-serif;
                }
                .lower-third_single-person .js-primary-line {
                    background:#ec6708;
                    color: #fff;
                }
                .lower-third_single-person .js-secondary-line {
                    color:#333;
                    background: #fff;
                }
            CSS;
        }

        if ($style == 'hannovercsd') {
            $css .= <<<CSS
            .lower-third_single-person .js-primary-line {
                background:#A2257C;
                color: #fff;
            }
            .lower-third_single-person .js-secondary-line {
                color:#A2257C;
                background: #fff;
            }
        CSS;
        }

        return $css;
    }
}
