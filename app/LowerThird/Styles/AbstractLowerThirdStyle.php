<?php


namespace App\LowerThird\Styles;

use App\LowerThird\Fields\AbstractFormField;
use Illuminate\Support\Collection;

abstract class AbstractLowerThirdStyle
{
    abstract static public function getKey() : string;
    abstract static public function getLabel() : string;

    /**
     * @return AbstractFormField[]|array
     */
    abstract public function getFields() : array;

    public function processValidatedData(array $data) {
        return $data;
    }

    public static function getCss(string $style = null) {
        return '';
    }

    public function getValidationRules() : array {
        return collect($this->getFields())
            ->mapWithKeys(function(AbstractFormField $field) {
                return [
                    'entry.' . $field->getKey() => $field->getRules()
                ];
            })
            ->all();
    }

    public function getValidationAttributeLabels() : array
    {
        return collect($this->getFields())
            ->mapWithKeys(function(AbstractFormField $field) {
                return [
                    'entry.' . $field->getKey() => $field->getLabel()
                ];
            })
            ->all();
    }
}
