<?php


namespace App\Services\Restream\Chat\Events;


class ChatEventFactory
{
    const EVENT_TYPE_TWITCH_TEXT = 4;
    const EVENT_TYPE_YOUTUBE_TEXT = 5;
    const EVENT_TYPE_FACEBOOK_PAGE_TEXT = 13;

    public static function fromMessagePayload(object $payload) : ChatEventInterface
    {
        switch ($payload->eventTypeId) {
            /**
             * Twitch Text
             */
            case self::EVENT_TYPE_TWITCH_TEXT:
                return FacebookPageText::fromMessagePayload($payload);
                break;
            /**
             * Youtube Text
             */
            case self::EVENT_TYPE_YOUTUBE_TEXT:
                return YoutubeTextEvent::fromMessagePayload($payload);
            /**
             * Facebook Page Text
             */
            case self::EVENT_TYPE_FACEBOOK_PAGE_TEXT:
                return FacebookPageText::fromMessagePayload($payload);
            default:
                return null;
        }
    }
}
