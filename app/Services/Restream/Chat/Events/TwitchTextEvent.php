<?php


namespace App\Services\Restream\Chat\Events;


use App\Services\Restream\Chat\ChatMessageSource;

class TwitchTextEvent extends AbstractChatEvent implements ChatEventInterface
{
    /**
     * @inheritDoc
     */
    public function getSource(): ChatMessageSource
    {
        return ChatMessageSource::TWITCH();
    }
}
