<?php


namespace App\Services\Restream\Chat\Events;


use App\Services\Restream\Chat\ChatMessageSource;

class FacebookPageText extends AbstractChatEvent implements ChatEventInterface
{
    /**
     * @inheritDoc
     */
    public function getSource(): ChatMessageSource
    {
        return ChatMessageSource::FACEBOOK_PAGE();
    }
}
