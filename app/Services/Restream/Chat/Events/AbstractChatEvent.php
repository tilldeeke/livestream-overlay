<?php


namespace App\Services\Restream\Chat\Events;


abstract class AbstractChatEvent implements ChatEventInterface
{
    protected $payload;

    private function __construct(object $payload)
    {
        $this->payload = $payload;
    }

    public static function fromMessagePayload(object $payload) : self
    {
        return new static($payload->eventPayload);
    }

    /**
     * @inheritDoc
     */
    public function getAuthorDisplayName(): string
    {
        if (!$this->payload->author) {
            return null;
        }

        return $this->payload->author->displayName;
    }

    /**
     * @inheritDoc
     */
    public function getAuthorAvatar(): string
    {
        if (!$this->payload->author) {
            return null;
        }

        return $this->payload->author->avatar;
    }

    /**
     * @inheritDoc
     */
    public function getText(): string
    {
        if (!$this->payload->text) {
            return null;
        }

        return $this->payload->text;
    }
}
