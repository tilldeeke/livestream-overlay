<?php


namespace App\Services\Restream\Chat\Events;


use App\Services\Restream\Chat\ChatMessageSource;

interface ChatEventInterface
{
    /**
     * The source of the event
     *
     * @return ChatMessageSource
     */
    public function getSource() : ChatMessageSource;

    /**
     * Returns a displayable name for the author of the event
     *
     * @return string|null
     */
    public function getAuthorDisplayName() : string;

    /**
     * Returns an avatar for the author of the event
     *
     * @return string|null
     */
    public function getAuthorAvatar() : string;

    /**
     * The content of the event, the message
     *
     * @return string
     */
    public function getText() : string;
}
