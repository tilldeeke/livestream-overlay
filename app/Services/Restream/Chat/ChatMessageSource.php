<?php

namespace App\Services\Restream\Chat;

use Spatie\Enum\Enum;

class ChatMessageSource extends Enum
{
    public static function TWITCH(): ChatMessageSource
    {
        return new class() extends ChatMessageSource {
            public function getName(): string
            {
                return 'Twitch';
            }
            public function getValue(): string
            {
                return 'twitch';
            }
        };
    }
    public static function YOUTUBE(): ChatMessageSource
    {
        return new class() extends ChatMessageSource {
            public function getName(): string
            {
                return 'Youtube';
            }
            public function getValue(): string
            {
                return 'youtube';
            }
        };
    }
    public static function FACEBOOK_PAGE(): ChatMessageSource
    {
        return new class() extends ChatMessageSource {
            public function getName(): string
            {
                return 'Facebook (Page)';
            }
            public function getValue(): string
            {
                return 'facebook-page';
            }
        };
    }
}
