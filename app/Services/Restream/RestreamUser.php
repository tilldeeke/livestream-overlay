<?php


namespace App\Services\Restream;


use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

class RestreamUser
{
    private $id;
    private $username;
    private $email;

    public function __construct($id, $username, $email)
    {
        $this->id = $id;
        $this->username = $username;
        $this->email = $email;
    }

    public static function fromArray(array $data)
    {
        return new static($data['id'], $data['username'], $data['email']);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }
}
