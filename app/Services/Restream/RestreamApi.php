<?php


namespace App\Services\Restream;


use App\Setting;
use GuzzleHttp\Client;
use League\OAuth2\Client\Token\AccessToken;
use League\OAuth2\Client\Token\AccessTokenInterface;

class RestreamApi
{
    /**
     * @var RestreamOAuth2Provider
     */
    private $provider;

    /**
     * @var AccessToken
     */
    private $token;

    /**
     * @var string
     */
    private $baseUrl = 'https://api.restream.io';


    /**
     * RestreamApi constructor.
     * @param RestreamOAuth2Provider $provider
     */
    public function __construct(RestreamOAuth2Provider $provider) {
        $this->provider = $provider;
        /*
        if ($token = Setting::get('restream_oauth_token')) {
            $this->token = new AccessToken($token);
        }
        */
    }

    public function getChatWebSocketUrl() : string
    {
        $token = $this->getValidAccessToken();

        return 'wss://chat.api.restream.io/ws?accessToken=' . $token;
    }

    private function getValidAccessToken() : AccessTokenInterface
    {
        $currentToken = new AccessToken(Setting::get('restream_oauth_token'));

        if (! $currentToken->hasExpired()) {
            return $currentToken;
        }

        $newToken = $this->provider->getAccessToken('refresh_token', [
            'refresh_token' => $currentToken->getRefreshToken()
        ]);

        Setting::set('restream_oauth_token', $newToken->jsonSerialize());

        return $newToken;
    }

    private function getAuthenticatedRequest($method, $url, array $options = []) {
        $token = $this->getValidAccessToken();

        return $this->provider->getAuthenticatedRequest($method, $url, $token, $options);
    }

    public function isAuthenticated() : bool {
        return Setting::get('restream_oauth_token') !== null;
    }

    /**
     * Sends a GET request and returns the parsed result
     *
     * @param $url
     * @param $options
     * @return mixed
     * @throws \League\OAuth2\Client\Provider\Exception\IdentityProviderException
     */
    private function get(string $url, array $options = []) {
        $request = $this->getAuthenticatedRequest(RestreamOAuth2Provider::METHOD_GET, $url, $options);

        return $this->provider->getParsedResponse($request);
    }

    /**
     * Sends a POST request and returns the parsed result
     *
     * @param $url
     * @param $options
     * @return mixed
     * @throws \League\OAuth2\Client\Provider\Exception\IdentityProviderException
     */
    private function post(string $url, array $options = []) {
        $request = $this->getAuthenticatedRequest(RestreamOAuth2Provider::METHOD_POST, $url, $options);

        return $this->provider->getParsedResponse($request);
    }

    /**
     * @param AccessToken $token
     * @throws \League\OAuth2\Client\Provider\Exception\IdentityProviderException
     */
    public function revokeToken(AccessToken $token) : void
    {
        $this->post($this->baseUrl . '/oauth/revoke', [
            'form_params' => [
                'token' => $token->getToken(),
                'token_type_hint' => 'access_token'
            ],
        ]);
    }

    /**
     * @return RestreamUser
     * @throws \League\OAuth2\Client\Provider\Exception\IdentityProviderException
     */
    public function getUser() : RestreamUser
    {
        $user = $this->get($this->baseUrl . '/v2/user/profile');

        return RestreamUser::fromArray($user);
    }
}
