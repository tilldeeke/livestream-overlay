<?php


namespace App\Services\Socialite;

use Illuminate\Support\Arr;
use Laravel\Socialite\Two\AbstractProvider;
use Laravel\Socialite\Contracts\Provider as ProviderContract;
use Laravel\Socialite\Two\User;

class RestreamIOProvider extends AbstractProvider implements ProviderContract
{

    /**
     * @inheritDoc
     */
    protected function getAuthUrl($state)
    {
        return $this->buildAuthUrlFromBase('https://api.restream.io/login', $state);
    }

    /**
     * @inheritDoc
     */
    protected function getTokenUrl()
    {
        return 'https://api.restream.io/oauth/token';
    }

    /**
     * @inheritDoc
     */
    protected function getUserByToken($token)
    {
        $userUrl = 'https://api.restream.io/v2/user/profile';

        $response = $this->getHttpClient()->get($userUrl, [
            'headers' => [
                'Authorization' => 'Bearer ' . $token
            ]
        ]);

        $user = json_decode($response->getBody(), true);

        return $user;
    }

    /**
     * @inheritDoc
     */
    protected function mapUserToObject(array $user)
    {
        return (new User)->setRaw($user)->map([
            'id' => $user['id'],
            'username' => $user['username'],
            'email' => Arr::get($user, 'email'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    protected function getTokenFields($code)
    {
        return parent::getTokenFields($code) + ['grant_type' => 'authorization_code'];
    }
}
