<?php


namespace App\Services\Socialite;

use Laravel\Socialite\SocialiteManager as DefaultManager;

class SocialiteManager extends DefaultManager
{
    /**
     * Create an instance of the specified driver.
     *
     * @return \Laravel\Socialite\Two\AbstractProvider
     */
    protected function createRestreamDriver()
    {
        $config = $this->app->make('config')['services.restream'];

        return $this->buildProvider(
            RestreamIOProvider::class, $config
        );
    }
}
