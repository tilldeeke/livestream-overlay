<?php

namespace App\Enums;

use Spatie\Enum\Enum;

class LowerThirdStatus extends Enum
{
    public static function ACTIVE(): LowerThirdStatus
    {
        return new class() extends LowerThirdStatus {
            public function getName(): string
            {
                return 'Sichtbar';
            }
            public function getValue(): string
            {
                return 'active';
            }
        };
    }

    public static function HIDDEN(): LowerThirdStatus
    {
        return new class() extends LowerThirdStatus {
            public function getName(): string
            {
                return 'Versteckt';
            }
            public function getValue(): string
            {
                return 'hidden';
            }
        };
    }
}
