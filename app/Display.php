<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Display extends Model
{
    protected $guarded = [];

    public function tallyLights() : BelongsToMany {
        return $this->belongsToMany(TallyLight::class, 'display_tally_lights');
    }
}
