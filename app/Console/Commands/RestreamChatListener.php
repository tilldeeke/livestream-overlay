<?php

namespace App\Console\Commands;

use App\ChatMessage;
use App\Services\Restream\Chat\Events\ChatEventFactory;
use App\Services\Restream\RestreamApi;
use Carbon\Carbon;
use Illuminate\Console\Command;
use WebSocket\Client;

class RestreamChatListener extends Command
{
    const ACTION_EVENT = 'event';
    const ACTION_HEARTBEAT = 'heartbeat';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'restream:chat:listen';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * @var RestreamApi
     */
    private $restreamApi;

    /**
     * @var Carbon
     */
    private $last_heartbeat_at = null;

    /**
     * Create a new command instance.
     *
     * @param RestreamApi $restreamApi
     */
    public function __construct(RestreamApi $restreamApi)
    {
        parent::__construct();
        $this->restreamApi = $restreamApi;
        $this->last_heartbeat_at = Carbon::now();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $client = new Client($this->restreamApi->getChatWebSocketUrl());

        while (true) {
            try {
                $message = $client->receive();
                // Act on received message
                $this->handleMessage($message);
                // Break while loop to stop listening
            } catch (\WebSocket\ConnectionException $e) {
                // Possibly log errors
            } catch(\Exception $e) {
                dd($e);
            }
        }
        $client->close();
    }

    /**
     * @param string $message
     */
    private function handleMessage(string $message)
    {
        $data = json_decode($message);

        switch ($data->action) {
            /**
             * Event, message received
             */
            case self::ACTION_EVENT:
                $event = ChatEventFactory::fromMessagePayload($data->payload);

                ChatMessage::create([
                    'source' => $event->getSource(),
                    'author_name' => $event->getAuthorDisplayName(),
                    'author_avatar' => $event->getAuthorAvatar(),
                    'message' => $event->getText()
                ]);

                break;
            /**
             * Heartbeat
             */
            case self::ACTION_HEARTBEAT:
                $this->handleHeartbeat($data);
                break;

            default:
                $this->warn('Unknown message received:');
                $this->line(json_encode($data));
                return;
        }
    }

    /**
     * @param $data
     */
    private function handleHeartbeat($data)
    {
        $this->warn('Heartbeat:');

        $new_heartbeat = Carbon::createFromTimestamp($data->timestamp);

        $this->line('Heartbeat at ' . $new_heartbeat);
        $this->line('Last heartbeat was ' . $this->last_heartbeat_at->diffForHumans($new_heartbeat));

        $this->last_heartbeat_at = $new_heartbeat;
    }
}
