<?php

namespace App\Providers;

use App\Services\Restream\RestreamApi;
use App\Services\Restream\RestreamOAuth2Provider;
use App\Services\Socialite\SocialiteManager;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;
use Laravel\Socialite\Contracts\Factory;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->singleton(RestreamOAuth2Provider::class, function ($app) {
            $config = $this->app->make('config')['services.restream'];

            return new RestreamOAuth2Provider([
                'clientId'      => $config['client_id'],
                'clientSecret'  => $config['client_secret'],
                'redirectUri'   => $config['redirect'],
            ]);
        });

        $this->app->singleton(RestreamApi::class, function ($app) {
            return new RestreamApi($this->app->make(RestreamOAuth2Provider::class));
        });

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
