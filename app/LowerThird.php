<?php

namespace App;

use App\Enums\AnimationIn;
use App\Enums\AnimationOut;
use App\Enums\LowerThirdStatus;
use App\Enums\LowerThirdStyle;
use App\Events\HideLowerThird;
use App\Events\LowerThirdCreated;
use App\Events\LowerThirdDeleted;
use App\Events\LowerThirdHidden;
use App\Events\LowerThirdShown;
use App\Events\ShowLowerThird;
use App\LowerThird\Styles\AbstractLowerThirdStyle;
use App\LowerThird\Styles\LowerThirdStyleRegistry;
use Illuminate\Database\Eloquent\Model;
use Spatie\Enum\Laravel\HasEnums;

/**
 * @method static findOrFail(int $id): LowerThird
 */
class LowerThird extends Model
{
    protected static $unguarded = true;

    protected $dispatchesEvents = [
        'created' => LowerThirdCreated::class,
        'deleted' => LowerThirdDeleted::class,
    ];

    public function getStyleAttribute() : ?AbstractLowerThirdStyle {
        return LowerThirdStyleRegistry::getByName($this->attributes['style']);
    }

    public function getStatusAttribute() : LowerThirdStatus {

        if ($this->attributes['status'] instanceof LowerThirdStatus) {
            return $this->attributes['status'];
        }

        return LowerThirdStatus::make($this->attributes['status']);
    }

    public function show() : void {
        $this->status = LowerThirdStatus::ACTIVE();
        $this->save();

        event(new LowerThirdShown($this));
    }
    public function hide() : void {
        $this->status = LowerThirdStatus::HIDDEN();
        $this->save();

        event(new LowerThirdHidden($this));
    }

    public function hasDataEntry(string $name) : bool {
        $properties = $this->getDataEntries();

        return isset($properties[$name]);
    }
    public function getDataEntries() : array {
        return json_decode($this->entry_data, true);
    }

    public function getDataEntry(string $name) : ?string {
        $properties = $this->getDataEntries();

        if (!isset($properties[$name])) {
            return null;
        }

        return $properties[$name];
    }
}
