<?php

namespace App\Http\Livewire;

use App\Enums\LowerThirdStatus;
use App\Events\HideLowerThird;
use App\Events\ShowLowerThird;
use App\LowerThird;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Livewire\Component;
use Livewire\WithPagination;

class LowerThirdList extends Component
{
    public $search = '';
    public $style = '';
    public $status = '';

    protected $queryString = [
        'search' => ['except' => ''],
        'style' => ['except' => ''],
        'status' => ['except' => ''],
    ];

    protected $listeners = [
        'echo:lower-third,.lower-third.show' => '$refresh',
        'echo:lower-third,.lower-third.hide' => '$refresh',

        'echo:lower-third,.created' => '$refresh',
        'echo:lower-third,.deleted' => '$refresh',
    ];

    public function mount(Request $request) {
        $this->fill($request->only(['search', 'style', 'status']));
    }

    public function show(int $id) {
        $entry = LowerThird::findOrFail($id);

        $entry->show();

        return;
    }
    public function hide(int $id) {
        $entry = LowerThird::findOrFail($id);

        $entry->hide();

        return;
    }

    public function delete(int $id) {
        $entry = LowerThird::findOrFail($id);

        $entry->delete();

        return;
    }

    public function render()
    {
        $entries = LowerThird::query()
            ->where(function(Builder $builder) {
                $builder->where('entry_data', 'LIKE', '%'.$this->search.'%');
            });

        if ($this->style !== '') {
            $entries = $entries->where('style', $this->style);
        }

        if ($this->status !== '') {
            $entries = $entries->where('status', $this->status);
        }

        return view('livewire.lower-third-list', [
            'entries' => $entries->get(),
            'search' => $this->search
        ]);
    }
}
