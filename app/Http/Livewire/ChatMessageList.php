<?php

namespace App\Http\Livewire;

use App\ChatMessage;
use Livewire\Component;

class ChatMessageList extends Component
{
    protected $listeners = [
        'echo:chat-messages,.created' => '$refresh',
    ];

    public function render()
    {
        return view('livewire.chat-message-list', [
            'messages' => ChatMessage::all()
        ]);
    }
}
