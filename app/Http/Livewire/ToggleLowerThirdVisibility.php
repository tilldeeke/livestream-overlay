<?php

namespace App\Http\Livewire;

use App\Events\HideLowerThird;
use App\Events\ShowLowerThird;
use App\LowerThird;
use Livewire\Component;

class ToggleLowerThirdVisibility extends Component
{
    /**
     * @var LowerThird
     */
    public $lowerThird;

    public function show() {
        $this->lowerThird->show();
    }
    public function hide() {
        $this->lowerThird->hide();
    }

    /**
     * @param LowerThird $lowerThird
     */
    public function mount(LowerThird $lowerThird) {
        $this->lowerThird = $lowerThird;
    }

    public function render()
    {
        return view('livewire.toggle-lower-third-visibility');
    }
}
