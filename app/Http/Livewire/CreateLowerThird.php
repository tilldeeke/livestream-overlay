<?php

namespace App\Http\Livewire;

use App\Enums\LowerThirdStatus;
use App\LowerThird;
use App\LowerThird\Styles\LowerThirdStyleRegistry;
use Livewire\Component;
use Livewire\WithFileUploads;

class CreateLowerThird extends Component
{
    use WithFileUploads;

    public $style = '';
    public $entry = [];

    public function submit() {

        $data = $this->validate([
            'style' => ['required']
        ]);

        $style = LowerThirdStyleRegistry::getByName($data['style']);

        $validatedData = $this->validate($style->getValidationRules(), [], $style->getValidationAttributeLabels());

        $data = $style->processValidatedData($validatedData['entry']);

        LowerThird::create([
            'style' => $style->getKey(),
            'status' => LowerThirdStatus::HIDDEN(),
            'entry_data' => json_encode($data)
        ]);
    }

    public function getFieldsProperty() : array {
        $style = LowerThirdStyleRegistry::getByName($this->style);

        if (!$style) {
            return [];
        }

        return $style->getFields();
    }

    public function render()
    {
        $availableStyles = LowerThirdStyleRegistry::getNames();

        return view('livewire.create-lower-third', [
            'availableStyles' => $availableStyles
        ]);
    }
}
