<?php

namespace App\Http\Controllers;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use League\Glide\Responses\LaravelResponseFactory;
use League\Glide\ServerFactory;

class GlideImageController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param Request $request
     * @param string $path
     * @return Response
     */
    public function __invoke(Request $request, string $path)
    {
        // Setup Glide server
        $server = ServerFactory::create([
            'source' => base_path('resources/img'),
            'cache' => storage_path('app/glide-cache'),
            'response' => new LaravelResponseFactory(app('request'))
        ]);

        return $server->getImageResponse($path, request()->all());
    }
}
