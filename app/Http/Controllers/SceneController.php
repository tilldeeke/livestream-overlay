<?php

namespace App\Http\Controllers;

use App\Scene;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class SceneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('scenes.index', [
            'scenes' => Scene::all()
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $data = $this->validate($request, [
            'scenes' => ['required'],
        ]);

        $scenes = explode(PHP_EOL, $data['scenes']);

        foreach($scenes as $name) {
            Scene::updateOrCreate(['name' => trim($name)], ['name' => trim($name)]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Scene  $scene
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Scene $scene)
    {
        $scene->delete();

        return redirect()->back();
    }
}
