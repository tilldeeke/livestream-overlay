<?php

namespace App\Http\Controllers;

use App\Countdown;
use App\Setting;
use Illuminate\Http\Request;

class CountdownDisplay extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function __invoke(Request $request)
    {
        $server_ip = Setting::get('obs_websocket_server_ip', '127.0.0.1');
        $server_port = Setting::get('obs_websocket_server_port', '4444');

        return view('countdowns.display', [
            'countdowns' => Countdown::all(),
            'server_ip' => $server_ip,
            'server_port' => $server_port
        ]);
    }
}
