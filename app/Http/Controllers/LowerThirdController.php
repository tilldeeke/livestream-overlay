<?php

namespace App\Http\Controllers;

use App\Enums\AnimationIn;
use App\Enums\AnimationOut;
use App\Events\HideLowerThird;
use App\Events\ShowLowerThird;
use App\LowerThird;
use Illuminate\Http\Request;

class LowerThirdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('lower-thirds.index');
    }

    public function display()
    {
        return view('lower-thirds.display');
    }

    /**
     * @param  \App\LowerThird  $lowerThird
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show(LowerThird $lowerThird)
    {
        $lowerThird->status = 'active';
        $lowerThird->save();

        event(new ShowLowerThird($lowerThird));

        return redirect()->back();
    }

    /**
     * @param  \App\LowerThird  $lowerThird
     * @return \Illuminate\Http\RedirectResponse
     */
    public function hide(LowerThird $lowerThird)
    {
        $lowerThird->status = 'hidden';
        $lowerThird->save();

        event(new HideLowerThird($lowerThird));

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $data = $this->validate($request, [
            'primary_line' => ['required'],
            'secondary_line' => ['nullable'],
            'style' => ['required']
        ]);

        LowerThird::create([
            'primary_line' => $request->input('primary_line'),
            'secondary_line' => $request->input('secondary_line'),
            'style' => $request->input('style'),
            'status' => 'hidden',
        ]);

        return redirect()->back();
    }

    /**
     * @param \App\LowerThird $lowerThird
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(LowerThird $lowerThird)
    {
        $lowerThird->delete();

        return redirect()->back();
    }
}
