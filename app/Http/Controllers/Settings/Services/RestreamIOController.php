<?php

namespace App\Http\Controllers\Settings\Services;

use App\Http\Controllers\Controller;
use App\Services\Restream\RestreamApi;
use App\Services\Restream\RestreamOAuth2Provider;
use App\Setting;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use League\OAuth2\Client\Token\AccessToken;

class RestreamIOController extends Controller
{
    /**
     * @var RestreamOAuth2Provider
     */
    private $provider;

    /**
     * RestreamIOController constructor.
     * @param RestreamOAuth2Provider $provider
     */
    public function __construct(RestreamOAuth2Provider $provider)
    {
        $this->provider = $provider;
    }

    /**
     * Redirect the user to the authentication page.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function connect(Request $request)
    {
        $url = $this->provider->getAuthorizationUrl();

        // Save state in session
        $state = $this->provider->getState();
        $request->session()->put('state', $state);

        return redirect()->to($url);
    }

    /**
     * Disconnect the account
     *
     * @param Request $request
     * @param RestreamApi $api
     * @return RedirectResponse
     * @throws \League\OAuth2\Client\Provider\Exception\IdentityProviderException
     */
    public function disconnect(Request $request, RestreamApi $api) {

        $token = new AccessToken(Setting::get('restream_oauth_token'));

        $api->revokeToken($token);

        Setting::set('restream_oauth_token', null);

        $request->session()->flash('success', 'Dienst Restream.io getrennt.');

        return redirect()->back();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function callback(Request $request)
    {
        if ($request->session()->get('state') !== $request->input('state')) {
            die;
        }

        // Try to get an access token (using the authorization code grant)
        $token = $this->provider->getAccessToken('authorization_code', [
            'code' => $request->input('code')
        ]);

        Setting::set('restream_oauth_token', $token->jsonSerialize());

        $request->session()->flash('success', 'Dienst Restream.io verbunden.');

        return redirect()->route('settings.services');
    }
}
