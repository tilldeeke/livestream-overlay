<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Services\Restream\RestreamApi;
use App\Services\Restream\RestreamOAuth2Provider;
use App\Setting;
use Illuminate\Http\Request;

class ServiceSettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function __invoke(RestreamApi $restreamApi)
    {
        return view('settings.services', [
            'restream' => $restreamApi
        ]);
    }
}
