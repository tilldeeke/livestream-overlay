<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Services\Restream\RestreamApi;
use App\Services\Restream\RestreamOAuth2Provider;
use App\Setting;
use Illuminate\Http\Request;

class GeneralSettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('settings.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {
        $data = $this->validate($request, [
            'obs_websocket_server_ip' => ['required'],
            'obs_websocket_server_port' => ['required', 'numeric', 'min:1', 'max:65536'],
            'lower_third_animation_in' => ['required'],
            'lower_third_animation_out' => ['required'],
            'lower_third_style' => ['required'],
        ]);

        foreach($data as $key => $value) {
            Setting::set($key, $value);
        }

        $request->session()->flash('success', 'Einstellungen gespeichert!');

        return redirect()->back();
    }
}
