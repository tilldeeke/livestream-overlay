<?php

namespace App\Http\Controllers;

use App\Scene;
use App\Setting;
use App\TallyLight;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class TallyLightController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('tally-lights.index', [
            'tallyLights' => TallyLight::all(),
            'scenes' => Scene::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $data = $this->validate($request, [
            'name' => ['required', 'string', 'max:255', Rule::unique((new TallyLight)->getTable())],
            'scenes' => ['required', 'array', 'min:1', Rule::exists('scenes', 'id')]
        ]);

        $light = TallyLight::create($request->only('name'));
        $light->scenes()->sync($request->input('scenes'));

        return redirect()->back();
    }

    /**
     * Show the specified resource.
     *
     * @param \App\TallyLight $tallyLight
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function show(TallyLight $tallyLight)
    {
        return view('tally-lights.show', [
            'tallyLight' => $tallyLight
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\TallyLight $tallyLight
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(TallyLight $tallyLight)
    {
        $tallyLight->delete();

        return redirect()->back();
    }
}
