<?php

namespace App\Http\Controllers;

use App\Display;
use App\TallyLight;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class DisplayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('displays.index', [
            'displays' => Display::all(),
            'tallyLights' => TallyLight::all()
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => ['required', Rule::unique('displays')],
            'tally_lights' => ['required', 'array'],
        ]);

        $display = Display::create([
            'name' => $request->input('name')
        ]);

        $display->tallyLights()->sync($request->input('tally_lights'));

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Display  $display
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Display $display)
    {
        return view('displays.show', ['display' => $display]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Display  $display
     * @return \Illuminate\Http\Response
     */
    public function destroy(Display $display)
    {
        $display->delete();

        return redirect()->back();
    }
}
