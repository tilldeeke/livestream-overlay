<?php

namespace App\Http\Controllers;

use App\Countdown;
use App\Scene;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class CountdownController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('countdowns.index', [
            'scenes' => Scene::all(),
            'countdowns' => Countdown::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $data = $this->validate($request, [
            'scene_id' => ['required', Rule::unique('countdowns', 'id')],
            'duration_in_seconds' => ['required', 'int', 'min:1', 'max:3600']
        ]);

        $countdown = Countdown::create($data);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Countdown $countdown
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Countdown $countdown)
    {
        $countdown->delete();

        return redirect()->back();
    }
}
