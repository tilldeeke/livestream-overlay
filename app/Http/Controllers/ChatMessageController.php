<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;

class ChatMessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response|\Illuminate\View\View
     */
    public function index()
    {
        return view('chat.index');
    }
}
