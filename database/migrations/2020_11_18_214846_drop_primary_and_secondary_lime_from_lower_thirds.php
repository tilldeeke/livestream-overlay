<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropPrimaryAndSecondaryLimeFromLowerThirds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lower_thirds', function (Blueprint $table) {
            $table->dropColumn('primary_line');
        });
        Schema::table('lower_thirds', function (Blueprint $table) {
            $table->dropColumn('secondary_line');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lower_thirds', function (Blueprint $table) {
            //
        });
    }
}
