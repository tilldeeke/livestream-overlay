<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTallyLightScenesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tally_light_scenes', function (Blueprint $table) {
            $table->integer('tally_light_id')->unsigned();
            $table->integer('scene_id')->unsigned();

            $table->foreign('tally_light_id')->references('id')->on('tally_lights')->onDelete('cascade');
            $table->foreign('scene_id')->references('id')->on('scenes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tally_light_scenes');
    }
}
