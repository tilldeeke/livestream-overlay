<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Display;
use Faker\Generator as Faker;

$factory->define(Display::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence(2)
    ];
});
