<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Countdown;
use Faker\Generator as Faker;

$factory->define(Countdown::class, function (Faker $faker) {
    return [
        'duration_in_seconds' => $faker->numberBetween(5, 180)
    ];
});
