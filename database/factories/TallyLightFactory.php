<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\TallyLight;
use Faker\Generator as Faker;

$factory->define(TallyLight::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->sentence
    ];
});
