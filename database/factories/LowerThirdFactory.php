<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\LowerThird;
use Faker\Generator as Faker;

$factory->define(LowerThird::class, function (Faker $faker) {
    return [
        'primary_line' => $faker->sentence(3),
        'secondary_line' => $faker->sentence(3),
        'style' => 'two-lines',
        'status' => 'hidden',
    ];
});

$factory->state(LowerThird::class, 'single-line', [
    'secondary_line' => null,
    'style' => 'single-line',
]);

$factory->state(LowerThird::class, 'cooperation', [
    'secondary_line' => null,
    'style' => 'cooperation',
]);

$factory->state(LowerThird::class, 'sponsors', [
    'secondary_line' => null,
    'style' => 'sponsors',
]);
