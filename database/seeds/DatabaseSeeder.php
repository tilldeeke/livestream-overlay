<?php

use App\Countdown;
use App\Display;
use App\LowerThird;
use App\Scene;
use App\Setting;
use App\TallyLight;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Setting::set('obs_websocket_server_ip', '127.0.0.1');
        Setting::set('obs_websocket_server_port', '4444');

        Setting::set('lower_third_animation_in', 'fadeIn');
        Setting::set('lower_third_animation_out', 'fadeOut');

        factory(Display::class, 3)->create();


        factory(LowerThird::class, 10)->create();
        factory(LowerThird::class, 5)->states('single-line')->create();
        factory(LowerThird::class, 1)->states('cooperation')->create();
        factory(LowerThird::class, 1)->states('sponsors')->create();

        $scenes = factory(Scene::class, 10)->create()->each(function(Scene $scene) {
            factory(Countdown::class)->create(['scene_id' => $scene->id]);
        });



        factory(TallyLight::class, 10)->create()->each(function (TallyLight $light) use ($scenes) {
            $light->scenes()->sync($scenes->random(random_int(1, $scenes->count())));
        });
    }
}
